<?php
// chemin du ficher XML repris sur https://www.isbn-international.org/range_file_generation
// Pour générer : `php ./isbn13_xml2php.php > isbn_map.php`

$file = 'isbn13/RangeMessage.xml';

if (file_exists($file)) {
	$xml = simplexml_load_file($file);

	$messageDate = $xml->MessageDate;
	$messageSerialNumber = $xml->MessageSerialNumber;

	echo "<?php
 
/* ***** BEGIN LICENSE BLOCK *****

Version: MPL 1.1

The contents of this file are subject to the Mozilla Public License Version
1.1 (the \"License\"); you may not use this file except in compliance with
the License. You may obtain a copy of the License at
http://www.mozilla.org/MPL/


Software distributed under the License is distributed on an \"AS IS\" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
for the specific language governing rights and limitations under the
License.

The Original Code is : EAN13.

The Initial Developer of the Original Code is Zéfling.
Portions created by the Initial Developer are Copyright (C) 2021
the Initial Developer. All Rights Reserved.

Contributor(s):
- Zéfling

***** END LICENSE BLOCK ***** */

/*
 * Conversion PHP du tableau pour les tirets
 * 
 * Source:
 * - Generated from RangeMessage.xml with isbn13_xml2php.php
 * - Available from https://www.isbn-international.org/range_file_generation
 * - MessageDate => $messageDate <br>
 * - MessageSerialNumber=> $messageSerialNumber
 */

/**
 * Liste des groupes ISBN
 * @author Zéfling (2021 - http://ikilote.net/fr/Blog/Techno-magis.html)
 * @version 2021-07-04
 */
final class ISBN_Map {
	public static \$groups_length = [\n";

	// UCC
 
	if(isset($xml->{'EAN.UCCPrefixes'}->{'EAN.UCC'})) {
		
		$ucc       = $xml->{'EAN.UCCPrefixes'}->{'EAN.UCC'};
		$ucc_count = count( $ucc );
		
		$list = [];		
		for($i = 0; $i < $ucc_count; $i++) {
			$groups = [];
			
			$rules       = $ucc[$i]->Rules->Rule;
			$rules_count = count( $rules );
			
			for($j = 0; $j < $rules_count; $j++) {
				list($min_str, $max_str) = explode('-', $rules[$j]->Range);
				$groups[] = [
					'min'    => intval($min_str),
					'max'    => intval($max_str),
					'length' => current($rules[$j]->Length),
				];
			}
			
			usort($groups, function ($a, $b) {
				return $a['max'] <=> $b['max'];
			});
						
			$list[current($ucc[$i]->Prefix)] = $groups;
		}
		krsort($list);
		
		$i       = 0;
		$i_count = count( $list );
		foreach($list as $prefix => $groups) {
			echo "		{$prefix} => [\n";
			$j       = 0;
			$j_count = count( $groups );
			foreach($groups as $group) {
				echo "			[
				'max' => ${group['max']},
				'length' => ${group['length']},
				'min' => ${group['min']}
			]";
				echo $j++ !== $j_count - 1 ? ",\n" : "\n";
			}
			echo $i++ !== $i_count - 1 ? "		],\n" : "		]\n";
		}
	}
	
	// Registration Groups
	
	echo	"	];
	
	public static \$publisher_length = [\n";
		
	if(isset($xml->RegistrationGroups->Group)) {
		
		$group       = $xml->RegistrationGroups->Group;
		$group_count = count( $group );
		
		$list = [];		
		for($i = 0; $i < $group_count; $i++) {
			$groups = [];
			
			$rules       = $group[$i]->Rules->Rule;
			$rules_count = count( $rules );
			
			for($j = 0; $j < $rules_count; $j++) {
				list($min_str, $max_str) = explode('-', $rules[$j]->Range);
				$length = $rules[$j]->Length;
				$groups[] = [
					'min'    => intval($min_str),
					'max'    => intval($max_str),
					'length' => current($rules[$j]->Length),
				];
			}
			
			usort($groups, function ($a, $b) {
				return $a['max'] <=> $b['max'];
			});
						
			$list[current($group[$i]->Prefix)] = $groups;
		}
		krsort($list);
		
		$i       = 0;
		$i_count = count( $list );
		foreach($list as $prefix => $groups) {
			echo "		'{$prefix}' => [\n";
			$j       = 0;
			$j_count = count( $groups );
			foreach($groups as $group) {
				echo "			[
				'max'    => ${group['max']},
				'length' => ${group['length']},
				'min'    => ${group['min']}
			]";
				echo $j !== $rules_count - 1 ? ",\n" : "\n";
			}
			echo $i !== $group_count - 1 ? "		],\n" : "		]\n";
		}
	}
	
}

echo '	];
}';
