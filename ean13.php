<?php

/* ***** BEGIN LICENSE BLOCK *****

Version: MPL 1.1

The contents of this file are subject to the Mozilla Public License Version
1.1 (the "License"); you may not use this file except in compliance with
the License. You may obtain a copy of the License at
http://www.mozilla.org/MPL/


Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
for the specific language governing rights and limitations under the
License.

The Original Code is : EAN13.

The Initial Developer of the Original Code is Zéfling.
Portions created by the Initial Developer are Copyright (C) 2014
the Initial Developer. All Rights Reserved.

Contributor(s):
- Zéfling

***** END LICENSE BLOCK ***** */

include_once 'isbn_map.php';

/**
 * Petite boîte à outils pour l'EAN13
 * @author Zéfling (2014 - http://ikilote.net/fr/Blog/Techno-magis.html)
 * @version 1.1 (2014-03-26)
 */
class EAN13 {

	/**
	 * Formate l'ISBN 13 à partir d'un nombre de 13 chiffres (portage)
	 * @param string $ean13 un nombre composé uniquement des chiffres
	 * @return string EAN 13 formaté XXX-X-XXXX-XXXX-X ou false si en erreur
	 * @see https://pypi.python.org/pypi/isbn_hyphenate 
	 */
	public static function format_ISBN ($ean13) {
		if (strlen($ean13) == 13) {
			preg_match('/(?<pre>...)(?<num>.........)(?<key>.)/', $ean13, $matches);
			
			// préfixe
			
			if (!isset(ISBN_Map::$groups_length[$matches['pre']])) {
				return false;
			}
			
			$avec = $matches['pre'].'-';

			// groupe
			
			$groupe_prefixe_length = null;
			$sept = (int) substr($matches['num'], 0 , 7);
			
			foreach(ISBN_Map::$groups_length[$matches['pre']] as &$ligne) {
				if ($ligne['min'] <= $sept && $sept <= $ligne['max']) {
					$groupe_prefixe_length = $ligne['length'];
					break;
				}
			}
			
			if (!$groupe_prefixe_length) {
				return false;
			}
			$group_prefix   = $matches['pre'].'-'.substr($matches['num'], 0, $groupe_prefixe_length);
			$matches['num'] = substr($matches['num'], $groupe_prefixe_length);
			
			// éditeur
			
			$sept = (int) sprintf("%0-7.7s",$matches['num']); // coupe à 7 caractères et ajoute des 0 à la fin si inféreur à 7
			$publisher_length = null;
						
			if (!isset(ISBN_Map::$publisher_length[$group_prefix])) {
				return false;
			}

			foreach(ISBN_Map::$publisher_length[$group_prefix] as &$ligne) {
				if ($ligne['min'] <= $sept && $sept <= $ligne['max']) {
					$publisher_length = $ligne['length'];
					break;
				}
			}
			
			if (!$publisher_length) {
				return false;
			}
			
			// rendu complet
			
			return $group_prefix.'-'.substr($matches['num'], 0, $publisher_length).'-'.
				substr($matches['num'], $publisher_length).'-'.$matches['key'];
		}
		return false;
	}
	
	/**
	 * génère la clef de vérification EAN 13
	 * @param string $ean13 EAN 13 (avec ou sans clef de contrôle)
	 * @return int la clef de contrôle (-1 si en erreur)
	 */
	public static function key ($ean13) {
		if (strlen($ean13) >= 12 && is_numeric($ean13)) {
			$n = (string) $ean13;
			return (10 - ( ($n[0] + $n[2] + $n[4] + $n[6] + $n[8] + $n[10]) + 
				($n[1] + $n[3] + $n[5] + $n[7] + $n[9]+ $n[11]) * 3 ) % 10) % 10;
		}
		return -1;
	}
	
	/**
	 * test si le numéro EAN 13 est correcte
	 * @param string $ean13 EAN 13  à vérifier
	 * @return boolean si la clef est valide
	 */
	public static function test ($ean13) {
		$ean13 = str_replace('-', '', trim($ean13));
		if (strlen($ean13) == 13 && is_numeric($ean13)) {
			return self::key($ean13) == $ean13[12];
		}
		return false;
	}
	
	/**
	 * génère un EAN13 aléatoirement dans une plage (avec possiblité d'exclusion)
	 * @param int $plage_debut 000 à 999
	 * @param int $plage_fin 000 à 999
	 * @param array $exclusion table d'exclusion (ne pas générer un code dans cette liste) (défaut : null) 
	 * @return string un EAN 13
	 */
	public static function generator ($plage_debut, $plage_fin, array &$exclusion = null) {
		if ($plage_fin > 999) $plage_fin = 999;
		if ($plage_debut < 1) $plage_debut = 1;
		if ($plage_debut > $plage_fin) $plage_fin = $plage_debut;
		
		if (!empty($exclusion)) {
			do {
				$ean13 = self::gen($plage_debut, $plage_fin);
			} while (in_array($ean13, $exclusion));
		} else {
			$ean13 = self::gen($plage_debut, $plage_fin);
		}
		return $ean13;
	}
	
	/**
	 * génère un EAN13 aléatoirement dans une plage (sans contrôle)
	 * @param int $plage_debut 000 à 999
	 * @param int $plage_fin 000 à 999
	 * @return string un EAN 13
	 */
	private static function gen ($plage_debut, $plage_fin) {
		
		$plage = ($plage_debut != $plage_fin) ? mt_rand($plage_debut, $plage_fin) : $plage_debut;
		$ean13 = sprintf("%1$03d%2$09d", $plage, mt_rand(0, 999999999));
		$ean13 .= self::key($ean13);
		
		return $ean13;
	}
	
}
